<?php

namespace MergeAfrica\Forum\Middleware;

use Closure;

/**
 * Class UserCanManageForumPosts
 * @package MergeAfrica\Forum\Middleware
 */
class UserCanManageForumPosts
{

    /**
     * Show 401 error if \Auth::user()->canManageForumPosts() == false
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::check()) {
            abort(401,"User not authorised to manage forum: You are not logged in");
        }
        if (!\Auth::user()->canManageForumPosts()) {
            abort(401,"User not authorised to manage forum: Your account is not authorised to forum.");
        }
        return $next($request);
    }
}
