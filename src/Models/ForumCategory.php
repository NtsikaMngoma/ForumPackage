<?php

namespace MergeAfrica\Forum\Models;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
    public $fillable = [
        'category_name',
        'slug',
        'category_description',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(ForumPost::class, 'forum_post_categories');
    }

    /**
     * Returns the public facing URL of showing forum posts in this category
     * @return string
     */
    public function url()
    {
        return route("forum.view_category", $this->slug);
    }

    /**
     * Returns the URL for an admin user to edit this category
     * @return string
     */
    public function edit_url()
    {
        return route("forum.admin.categories.edit_category", $this->id);
    }

//    public function scopeApproved($query)
//    {
//        dd("A");
//        return $query->where("approved", true);
//    }
}
