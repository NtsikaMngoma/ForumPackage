<?php

namespace MergeAfrica\Forum\Models;

use Illuminate\Database\Eloquent\Model;

class ForumUploadedPhoto extends Model
{
    public $table = 'forum_uploaded_photos';
    public $casts = [
        'uploaded_images' => 'array',
    ];
    public $fillable = [

        'image_title',
        'uploader_id',
        'source', 'uploaded_images',
    ];
}
