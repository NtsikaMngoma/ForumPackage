<?php

namespace MergeAfrica\Forum;

use Illuminate\Support\ServiceProvider;
use Swis\LaravelFulltext\ModelObserver;
use MergeAfrica\Forum\Models\ForumPost;

class ForumServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        if (config("forum.search.search_enabled") == false) {
            // if search is disabled, don't allow it to sync.
            ModelObserver::disableSyncingFor(ForumPost::class);
        }

        if (config("forum.include_default_routes", true)) {
            include(__DIR__ . "/routes.php");
        }


        foreach ([
                     '2018_05_28_224023_create_forum_posts_table.php',
                     '2018_09_16_224023_add_author_and_url_forum_posts_table.php',
                     '2018_09_26_085711_add_short_desc_textarea_to_forum.php',
                     '2018_09_27_122627_create_forum_uploaded_photos_table.php'
                 ] as $file) {

            $this->publishes([
                __DIR__ . '/../migrations/' . $file => database_path('migrations/' . $file)
            ]);

        }

        $this->publishes([
            __DIR__ . '/Views/forum' => base_path('resources/views/vendor/forum'),
            __DIR__ . '/Config/forum.php' => config_path('forum.php'),
            __DIR__ . '/css/forum_admin_css.css' => public_path('forum_admin_css.css'),
        ]);


    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        // for the admin backend views ( view("forum_admin::BLADEFILE") )
        $this->loadViewsFrom(__DIR__ . "/Views/forum_admin", 'forum_admin');

        // for public facing views (view("forum::BLADEFILE")):
        // if you do the vendor:publish, these will be copied to /resources/views/vendor/forum anyway
        $this->loadViewsFrom(__DIR__ . "/Views/forum", 'forum');
    }

}
