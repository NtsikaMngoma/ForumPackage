@if(\Auth::check() && \Auth::user()->canManageForumPosts())
    <a href="{{$post->edit_url()}}" class="btn btn-outline-secondary btn-sm pull-right float-right">Edit
        Post</a>
@endif

<h2 class='forum_title'>{{$post->title}}</h2>
<h5 class='forum_subtitle'>{{$post->subtitle}}</h5>


<?=$post->image_tag("medium", false, 'd-block mx-auto'); ?>

<p class="forum_body_content">
    {!! $post->post_body_output() !!}

    {{--@if(config("forum.use_custom_view_files")  && $post->use_view_file)--}}
    {{--                                // use a custom blade file for the output of those forum post--}}
    {{--   @include("forum::partials.use_view_file")--}}
    {{--@else--}}
    {{--   {!! $post->post_body !!}        // unsafe, echoing the plain html/js--}}
    {{--   {{ $post->post_body }}          // for safe escaping --}}
    {{--@endif--}}
</p>

<hr/>

Posted <strong>{{$post->posted_at->diffForHumans()}}</strong>

@includeWhen($post->author,"forum::partials.author",['post'=>$post])
@includeWhen($post->categories,"forum::partials.categories",['post'=>$post])
