@extends("layouts.app",['title'=>$title])
@section("content")

    {{--https://webdevetc.com/laravel/packages/forum-forum-system-for-your-laravel-app/help-documentation/laravel-forum-package-forum#guide_to_views--}}

    <div class='row'>
        <div class='col-sm-12 forum_container'>
            @if(\Auth::check() && \Auth::user()->canManageForumPosts())
                <div class="text-center">
                        <p class='mb-1'>You are logged in as a forum admin user.
                            <br>

                            <a href='{{route("forum.admin.index")}}'
                               class='btn border  btn-outline-primary btn-sm '>

                                <i class="fa fa-cogs" aria-hidden="true"></i>

                                Go To Forum Admin Panel</a>


                        </p>
                </div>
            @endif


            @if(isset($forum_category) && $forum_category)
                <h2 class='text-center'>Viewing Category: {{$forum_category->category_name}}</h2>

                @if($forum_category->category_description)
                    <p class='text-center'>{{$forum_category->category_description}}</p>
                @endif

            @endif


            @forelse($posts as $post)
                @include("forum::partials.index_loop")
            @empty
                <div class='alert alert-danger'>No posts</div>
            @endforelse

            <div class='text-center  col-sm-4 mx-auto'>
                {{$posts->appends( [] )->links()}}
            </div>




                @include("forum::sitewide.search_form")

        </div>
    </div>
@endsection