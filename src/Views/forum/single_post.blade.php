@extends("layouts.app",['title'=>$post->gen_seo_title()])
@section("content")


    {{--https://webdevetc.com/laravel/packages/forum-forum-system-for-your-laravel-app/help-documentation/laravel-forum-package-forum#guide_to_views--}}

    <div class='container'>
    <div class='row'>
        <div class='col-sm-12 col-md-12 col-lg-12'>

            @include("forum::partials.show_errors")
            @include("forum::partials.full_post_details")


            @if(config("forum.comments.type_of_comments_to_show","built_in") !== 'disabled')
                <div class="" id='maincommentscontainer'>
                    <h2 class='text-center' id='forumcomments'>Comments</h2>
                    @include("forum::partials.show_comments")
                </div>
            @else
                {{--Comments are disabled--}}
            @endif


        </div>
    </div>
    </div>

@endsection