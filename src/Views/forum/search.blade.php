@extends("layouts.app",['title'=>$title])
@section("content")

    {{--https://webdevetc.com/laravel/packages/forum-forum-system-for-your-laravel-app/help-documentation/laravel-forum-package-forum#guide_to_views--}}

    <div class='row'>
        <div class='col-sm-12'>
            <h2>Search Results for {{$query}}</h2>

            @forelse($search_results as $result)

                <?php $post = $result->indexable; ?>
                @if($post && is_a($post,\MergeAfrica\Forum\Models\ForumPost::class))
                    <h2>Search result #{{$loop->count}}</h2>
                    @include("forum::partials.index_loop")
                @else

                    <div class='alert alert-danger'>Unable to show this search result - unknown type</div>
                @endif
            @empty
                <div class='alert alert-danger'>Sorry, but there were no results!</div>
            @endforelse


            @include("forum::sitewide.search_form")

        </div>
    </div>


@endsection