@extends("forum_admin")
@section("content")


    <h5>Admin - Add post</h5>

    <form method='post' action='{{route("forum.admin.store_post")}}'  enctype="multipart/form-data" >

        @csrf
        @include("forum_admin::posts.form", ['post' => new \MergeAfrica\Forum\Models\ForumPost()])

        <input type='submit' class='btn btn-primary' value='Add new post' >

    </form>

@endsection