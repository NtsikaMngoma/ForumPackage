@extends("forum_admin")
@section("content")


    <h5>Admin - Edit Category</h5>

    <form method='post' action='{{route("forum.admin.categories.edit_category",$category->id)}}'  enctype="multipart/form-data" >

        @csrf
        @method("patch")
        @include("forum_admin::categories.form", ['category' => $category])

        <input type='submit' class='btn btn-primary' value='Save Changes' >

    </form>

@endsection