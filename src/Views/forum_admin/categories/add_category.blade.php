@extends("forum_admin")
@section("content")


    <h5>Admin - Add Category</h5>

    <form method='post' action='{{route("forum.admin.categories.create_category")}}'  enctype="multipart/form-data" >

        @csrf
        @include("forum_admin", ['category' => new \MergeAfrica\Forum\Models\ForumCategory()])

        <input type='submit' class='btn btn-primary' value='Add new category' >

    </form>

@endsection