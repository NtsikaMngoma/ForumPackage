<?php

Route::group(['middleware' => ['web'], 'namespace' => '\MergeAfrica\Forum\Controllers'], function () {


    /** The main public facing forum routes - show all posts, view a category, rss feed, view a single post, also the add comment route */
    Route::group(['prefix' => config('forum.forum_prefix', 'forum')], function () {

        Route::get('/', 'ForumReaderController@index')
            ->name('forum.index');

        Route::get('/search', 'ForumReaderController@search')
            ->name('forum.search');

        Route::get('/feed', 'ForumRssFeedController@feed')
            ->name('forum.feed'); //RSS feed

        Route::get('/category/{categorySlug}',
            'ForumReaderController@view_category')
            ->name('forum.view_category');

        Route::get('/{forumPostSlug}',
            'ForumReaderController@viewSinglePost')
            ->name('forum.single');


        // throttle to a max of 10 attempts in 3 minutes:
        Route::group(['middleware' => 'throttle:10,3'], function () {

            Route::post('save_comment/{forumPostSlug}',
                'ForumCommentWriterController@addNewComment')
                ->name('forum.comments.add_new_comment');


        });

    });


    /* Admin backend routes - CRUD for posts, categories, and approving/deleting submitted comments */
    Route::group(['prefix' => config('forum.admin_prefix', 'forum_admin')], function () {

        Route::get('/', 'ForumAdminController@index')
            ->name('forum.admin.index');

        Route::get('/add_post',
            'ForumAdminController@create_post')
            ->name('forum.admin.create_post');


        Route::post('/add_post',
            'ForumAdminController@store_post')
            ->name('forum.admin.store_post');


        Route::get('/edit_post/{forumPostId}',
            'ForumAdminController@edit_post')
            ->name('forum.admin.edit_post');

        Route::patch('/edit_post/{forumPostId}',
            'ForumAdminController@update_post')
            ->name('forum.admin.update_post');


        Route::group(['prefix' => "image_uploads",], function () {

            Route::get("/", "ForumImageUploadController@index")->name("forum.admin.images.all");

            Route::get("/upload", "ForumImageUploadController@create")->name("forum.admin.images.upload");
            Route::post("/upload", "ForumImageUploadController@store")->name("forum.admin.images.store");

        });


        Route::delete('/delete_post/{forumPostId}',
            'ForumAdminController@destroy_post')
            ->name('forum.admin.destroy_post');

        Route::group(['prefix' => 'comments',], function () {

            Route::get('/',
                'ForumCommentsAdminController@index')
                ->name('forum.admin.comments.index');

            Route::patch('/{commentId}',
                'ForumCommentsAdminController@approve')
                ->name('forum.admin.comments.approve');
            Route::delete('/{commentId}',
                'ForumCommentsAdminController@destroy')
                ->name('forum.admin.comments.delete');
        });

        Route::group(['prefix' => 'categories'], function () {

            Route::get('/',
                'ForumCategoryAdminController@index')
                ->name('forum.admin.categories.index');

            Route::get('/add_category',
                'ForumCategoryAdminController@create_category')
                ->name('forum.admin.categories.create_category');
            Route::post('/add_category',
                'ForumCategoryAdminController@store_category')
                ->name('forum.admin.categories.store_category');

            Route::get('/edit_category/{categoryId}',
                'ForumCategoryAdminController@edit_category')
                ->name('forum.admin.categories.edit_category');

            Route::patch('/edit_category/{categoryId}',
                'ForumCategoryAdminController@update_category')
                ->name('forum.admin.categories.update_category');

            Route::delete('/delete_category/{categoryId}',
                'ForumCategoryAdminController@destroy_category')
                ->name('forum.admin.categories.destroy_category');

        });

    });
});

