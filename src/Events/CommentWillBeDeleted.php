<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumComment;

/**
 * Class CommentWillBeDeleted
 * @package MergeAfrica\Forum\Events
 */
class CommentWillBeDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumComment */
    public $comment;

    /**
     * CommentWillBeDeleted constructor.
     * @param ForumComment $comment
     */
    public function __construct(ForumComment $comment)
    {
        $this->comment=$comment;
    }

}
