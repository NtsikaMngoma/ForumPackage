<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumComment;
use MergeAfrica\Forum\Models\ForumPost;

/**
 * Class CommentAdded
 * @package MergeAfrica\Forum\Events
 */
class CommentAdded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumPost */
    public $forumPost;
    /** @var  ForumComment */
    public $newComment;

    /**
     * CommentAdded constructor.
     * @param ForumPost $forumPost
     * @param ForumComment $newComment
     */
    public function __construct(ForumPost $forumPost, ForumComment $newComment)
    {
        $this->forumPost=$forumPost;
        $this->newComment=$newComment;
    }

}
