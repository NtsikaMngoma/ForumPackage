<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumPost;

/**
 * Class ForumPostEdited
 * @package MergeAfrica\Forum\Events
 */
class ForumPostEdited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumPost */
    public $forumPost;

    /**
     * ForumPostEdited constructor.
     * @param ForumPost $forumPost
     */
    public function __construct(ForumPost $forumPost)
    {
        $this->forumPost=$forumPost;
    }

}
