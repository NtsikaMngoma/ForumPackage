<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumPost;

/**
 * Class ForumPostWillBeDeleted
 * @package MergeAfrica\Forum\Events
 */
class ForumPostWillBeDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumPost */
    public $forumPost;

    /**
     * ForumPostWillBeDeleted constructor.
     * @param ForumPost $forumPost
     */
    public function __construct(ForumPost $forumPost)
    {
        $this->forumPost=$forumPost;
    }

}
