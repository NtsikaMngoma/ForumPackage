<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumComment;

/**
 * Class CommentApproved
 * @package MergeAfrica\Forum\Events
 */
class CommentApproved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumComment */
    public $comment;

    /**
     * CommentApproved constructor.
     * @param ForumComment $comment
     */
    public function __construct(ForumComment $comment)
    {
        $this->comment=$comment;
        // you can get the forum post via $comment->post
    }

}
