<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumCategory;

/**
 * Class CategoryWillBeDeleted
 * @package MergeAfrica\Forum\Events
 */
class CategoryWillBeDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumCategory */
    public $forumCategory;

    /**
     * CategoryWillBeDeleted constructor.
     * @param ForumCategory $forumCategory
     */
    public function __construct(ForumCategory $forumCategory)
    {
        $this->forumCategory=$forumCategory;
    }

}
