<?php

namespace MergeAfrica\Forum\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use MergeAfrica\Forum\Models\ForumPost;

/**
 * Class UploadedImage
 * @package MergeAfrica\Forum\Events
 */
class UploadedImage
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var  ForumPost|null */
    public $forumPost;
    /**
     * @var
     */
    public $image;

    public $source;
    public $image_filename;

    /**
     * UploadedImage constructor.
     *
     * @param $image_filename - the new filename
     * @param ForumPost $forumPost
     * @param $image
     * @param $source string|null  the __METHOD__  firing this event (or other string)
     */
    public function __construct(string $image_filename, $image, ForumPost $forumPost=null, string $source='other')
    {
        $this->image_filename = $image_filename;
        $this->forumPost=$forumPost;
        $this->image=$image;
        $this->source=$source;
    }

}
