<?php

namespace MergeAfrica\Forum\Requests;

use Illuminate\Foundation\Http\FormRequest;
use MergeAfrica\Forum\Interfaces\BaseRequestInterface;

/**
 * Class BaseRequest
 * @package MergeAfrica\Forum\Requests
 */
abstract class BaseRequest extends FormRequest implements BaseRequestInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check() && \Auth::user()->canManageForumPosts();
    }
}
