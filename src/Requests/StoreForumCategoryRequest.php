<?php
namespace MergeAfrica\Forum\Requests;


use Illuminate\Validation\Rule;

class StoreForumCategoryRequest extends BaseForumCategoryRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = $this->baseCategoryRules();
        $return['slug'] [] = Rule::unique("forum_categories", "slug");
        return $return;
    }
}
