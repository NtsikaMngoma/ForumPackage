<?php

namespace MergeAfrica\Forum\Requests;


use Illuminate\Validation\Rule;
use MergeAfrica\Forum\Requests\Traits\HasCategoriesTrait;
use MergeAfrica\Forum\Requests\Traits\HasImageUploadTrait;

class CreateForumPostRequest extends BaseForumPostRequest
{
    use HasCategoriesTrait;
    use HasImageUploadTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = $this->baseForumRules();
        $return['slug'] [] = Rule::unique("forum_posts", "slug");
        return $return;
    }

}
