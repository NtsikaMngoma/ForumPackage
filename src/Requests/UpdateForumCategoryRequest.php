<?php

namespace MergeAfrica\Forum\Requests;


use Illuminate\Validation\Rule;
use MergeAfrica\Forum\Models\ForumCategory;

class UpdateForumCategoryRequest extends BaseForumCategoryRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = $this->baseCategoryRules();
        $return['slug'] [] = Rule::unique("forum_categories", "slug")->ignore($this->route()->parameter("categoryId"));
        return $return;

    }
}
