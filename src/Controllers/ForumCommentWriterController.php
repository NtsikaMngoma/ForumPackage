<?php

namespace MergeAfrica\Forum\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use MergeAfrica\Forum\Captcha\CaptchaAbstract;
use MergeAfrica\Forum\Captcha\UsesCaptcha;
use MergeAfrica\Forum\Events\CommentAdded;
use MergeAfrica\Forum\Models\ForumComment;
use MergeAfrica\Forum\Models\ForumPost;
use MergeAfrica\Forum\Requests\AddNewCommentRequest;

/**
 * Class ForumCommentWriterController
 * @package MergeAfrica\Forum\Controllers
 */
class ForumCommentWriterController extends Controller
{

    use UsesCaptcha;

    /**
     * Let a guest (or logged in user) submit a new comment for a forum post
     *
     * @param AddNewCommentRequest $request
     * @param $forum_post_slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function addNewComment(AddNewCommentRequest $request, $forum_post_slug)
    {

        if (config("forum.comments.type_of_comments_to_show", "built_in") !== 'built_in') {
            throw new \RuntimeException("Built in comments are disabled");
        }

        $forum_post = ForumPost::where("slug", $forum_post_slug)
            ->firstOrFail();

        /** @var CaptchaAbstract $captcha */
        $captcha = $this->getCaptchaObject();
        if ($captcha) {
            $captcha->runCaptchaBeforeAddingComment($request, $forum_post);
        }

        $new_comment = $this->createNewComment($request, $forum_post);

        return view("forum::saved_comment", [
            'captcha' => $captcha,
            'forum_post' => $forum_post,
            'new_comment' => $new_comment
        ]);

    }

    /**
     * @param AddNewCommentRequest $request
     * @param $forum_post
     * @return ForumComment
     */
    protected function createNewComment(AddNewCommentRequest $request, $forum_post)
    {
        $new_comment = new ForumComment($request->all());

        if (config("forum.comments.save_ip_address")) {
            $new_comment->ip = $request->ip();
        }
        if (config("forum.comments.ask_for_author_website")) {
            $new_comment->author_website = $request->get('author_website');
        }
        if (config("forum.comments.ask_for_author_website")) {
            $new_comment->author_email = $request->get('author_email');
        }
        if (config("forum.comments.save_user_id_if_logged_in", true) && Auth::check()) {
            $new_comment->user_id = Auth::user()->id;
        }

        $new_comment->approved = config("forum.comments.auto_approve_comments", true) ? true : false;

        $forum_post->comments()->save($new_comment);

        event(new CommentAdded($forum_post, $new_comment));

        return $new_comment;
    }

}
