<?php

namespace MergeAfrica\Forum\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MergeAfrica\Forum\Interfaces\BaseRequestInterface;
use MergeAfrica\Forum\Events\ForumPostAdded;
use MergeAfrica\Forum\Events\ForumPostEdited;
use MergeAfrica\Forum\Events\ForumPostWillBeDeleted;
use MergeAfrica\Forum\Helpers;
use MergeAfrica\Forum\Middleware\UserCanManageForumPosts;
use MergeAfrica\Forum\Models\ForumPost;
use MergeAfrica\Forum\Models\ForumUploadedPhoto;
use MergeAfrica\Forum\Requests\CreateForumPostRequest;
use MergeAfrica\Forum\Requests\DeleteForumPostRequest;
use MergeAfrica\Forum\Requests\UpdateForumPostRequest;
use MergeAfrica\Forum\Traits\UploadFileTrait;

/**
 * Class ForumAdminController
 * @package MergeAfrica\Forum\Controllers
 */
class ForumAdminController extends Controller
{
    use UploadFileTrait;

    /**
     * ForumAdminController constructor.
     */
    public function __construct()
    {
        $this->middleware(UserCanManageForumPosts::class);

        if (!is_array(config("forum"))) {
            throw new \RuntimeException('The config/forum.php does not exist. Publish the vendor files for the forum package by running the php artisan publish:vendor command');
        }
    }


    /**
     * View all posts
     *
     * @return mixed
     */
    public function index()
    {
        $posts = ForumPost::orderBy("posted_at", "desc")
            ->paginate(10);

        return view("forum_admin::index", ['posts'=>$posts]);
    }

    /**
     * Show form for creating new post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_post()
    {
        return view("forum_admin::posts.add_post");
    }

    /**
     * Save a new post
     *
     * @param CreateForumPostRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store_post(CreateForumPostRequest $request)
    {
        $new_forum_post = new ForumPost($request->all());

        $this->processUploadedImages($request, $new_forum_post);

        if (!$new_forum_post->posted_at) {
            $new_forum_post->posted_at = Carbon::now();
        }

        $new_forum_post->user_id = \Auth::user()->id;
        $new_forum_post->save();

        $new_forum_post->categories()->sync($request->categories());

        Helpers::flash_message("Added post");
        event(new ForumPostAdded($new_forum_post));
        return redirect($new_forum_post->edit_url());
    }

    /**
     * Show form to edit post
     *
     * @param $forumPostId
     * @return mixed
     */
    public function edit_post( $forumPostId)
    {
        $post = ForumPost::findOrFail($forumPostId);
        return view("forum_admin::posts.edit_post")->withPost($post);
    }

    /**
     * Save changes to a post
     *
     * @param UpdateForumPostRequest $request
     * @param $forumPostId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update_post(UpdateForumPostRequest $request, $forumPostId)
    {
        /** @var ForumPost $post */
        $post = ForumPost::findOrFail($forumPostId);
        $post->fill($request->all());

        $this->processUploadedImages($request, $post);

        $post->save();
        $post->categories()->sync($request->categories());

        Helpers::flash_message("Updated post");
        event(new ForumPostEdited($post));

        return redirect($post->edit_url());

    }

    /**
     * Delete a post
     *
     * @param DeleteForumPostRequest $request
     * @param $forumPostId
     * @return mixed
     */
    public function destroy_post(DeleteForumPostRequest $request, $forumPostId)
    {

        $post = ForumPost::findOrFail($forumPostId);
        event(new ForumPostWillBeDeleted($post));

        $post->delete();

        // todo - delete the featured images?
        // At the moment it just issues a warning saying the images are still on the server.

        return view("forum_admin::posts.deleted_post")
            ->withDeletedPost($post);

    }

    /**
     * Process any uploaded images (for featured image)
     *
     * @param BaseRequestInterface $request
     * @param $new_forum_post
     * @throws \Exception
     * @todo - next full release, tidy this up!
     */
    protected function processUploadedImages(BaseRequestInterface $request, ForumPost $new_forum_post)
    {
        if (!config("forum.image_upload_enabled")) {
            // image upload was disabled
            return;
        }

        $this->increaseMemoryLimit();

        // to save in db later
        $uploaded_image_details = [];


        foreach ((array)config('forum.image_sizes') as $size => $image_size_details) {

            if ($image_size_details['enabled'] && $photo = $request->get_image_file($size)) {
                // this image size is enabled, and
                // we have an uploaded image that we can use

                $uploaded_image = $this->UploadAndResize($new_forum_post, $new_forum_post->title, $image_size_details, $photo);

                $new_forum_post->$size = $uploaded_image['filename'];
                $uploaded_image_details[$size] = $uploaded_image;
            }
        }


        // store the image upload.
        // todo: link this to the forum_post row.
        if (count(array_filter($uploaded_image_details))>0) {
            ForumUploadedPhoto::create([
                'source' => "ForumFeaturedImage",
                'uploaded_images' => $uploaded_image_details,
            ]);
        }


    }


}
