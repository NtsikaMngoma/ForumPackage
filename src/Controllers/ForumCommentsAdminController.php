<?php

namespace MergeAfrica\Forum\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MergeAfrica\Forum\Events\CommentApproved;
use MergeAfrica\Forum\Events\CommentWillBeDeleted;
use MergeAfrica\Forum\Helpers;
use MergeAfrica\Forum\Middleware\UserCanManageForumPosts;
use MergeAfrica\Forum\Models\ForumComment;

/**
 * Class ForumCommentsAdminController
 * @package MergeAfrica\Forum\Controllers
 */
class ForumCommentsAdminController extends Controller
{
    /**
     * ForumCommentsAdminController constructor.
     */
    public function __construct()
    {
        $this->middleware(UserCanManageForumPosts::class);
    }

    /**
     * Show all comments (and show buttons with approve/delete)
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $comments = ForumComment::withoutGlobalScopes()->orderBy("created_at", "desc")
            ->with("post");

        if ($request->get("waiting_for_approval")) {
            $comments->where("approved", false);
        }

        $comments = $comments->paginate(100);
        return view("forum_admin::comments.index")
            ->withComments($comments
            );
    }


    /**
     * Approve a comment
     *
     * @param $forumCommentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($forumCommentId)
    {
        $comment = ForumComment::withoutGlobalScopes()->findOrFail($forumCommentId);
        $comment->approved = true;
        $comment->save();

        Helpers::flash_message("Approved!");
        event(new CommentApproved($comment));

        return back();

    }

    /**
     * Delete a submitted comment
     *
     * @param $forumCommentId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($forumCommentId)
    {
        $comment = ForumComment::withoutGlobalScopes()->findOrFail($forumCommentId);
        event(new CommentWillBeDeleted($comment));

        $comment->delete();

        Helpers::flash_message("Deleted!");
        return back();
    }


}
