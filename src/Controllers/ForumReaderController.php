<?php

namespace MergeAfrica\Forum\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Swis\LaravelFulltext\Search;
use MergeAfrica\Forum\Captcha\UsesCaptcha;
use MergeAfrica\Forum\Models\ForumCategory;
use MergeAfrica\Forum\Models\ForumPost;

/**
 * Class ForumReaderController
 * All of the main public facing methods for viewing forum content (index, single posts)
 * @package MergeAfrica\Forum\Controllers
 */
class ForumReaderController extends Controller
{
    use UsesCaptcha;

    /**
     * Show forum posts
     * If category_slug is set, then only show from that category
     *
     * @param null $category_slug
     * @return mixed
     */
    public static function index($category_slug = null)
    {
        // the published_at + is_published are handled by ForumPublishedScope, and don't take effect if the logged in user can manageb log posts
        $title = 'Viewing forum'; // default title...

        if ($category_slug) {
            $category = ForumCategory::where("slug", $category_slug)->firstOrFail();
            $posts = $category->posts()->where("forum_post_categories.forum_category_id", $category->id);

            // at the moment we handle this special case (viewing a category) by hard coding in the following two lines.
            // You can easily override this in the view files.
            \View::share('forum_category', $category); // so the view can say "You are viewing $CATEGORYNAME category posts"
            $title = 'Viewing posts in ' . $category->category_name . " category"; // hardcode title here...
        } else {
            $posts = ForumPost::query();
        }

        $posts = $posts->orderBy("posted_at", "desc")
            ->paginate(config("forum.per_page", 10));

        return view("forum::index", [
            'posts' => $posts,
            'title' => $title,
        ]);
    }

    /**
     * Show the search results for $_GET['s']
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function search(Request $request)
    {
        if (!config("forum.search.search_enabled")) {
            throw new \Exception("Search is disabled");
        }
        $query = $request->get("s");
        $search = new Search();
        $search_results = $search->run($query);

        \View::share("title", "Search results for " . e($query));

        return view("forum::search", ['query' => $query, 'search_results' => $search_results]);

    }




    /**
     * View all posts in $category_slug category
     *
     * @param Request $request
     * @param $category_slug
     * @return mixed
     */
    public function view_category($category_slug)
    {
        return $this->index($category_slug);
    }

    /**
     * View a single post and (if enabled) it's comments
     *
     * @param Request $request
     * @param $forumPostSlug
     * @return mixed
     */
    public function viewSinglePost(Request $request, $forumPostSlug)
    {
        // the published_at + is_published are handled by ForumPublishedScope, and don't take effect if the logged in user can manage log posts
        $forum_post = ForumPost::where("slug", $forumPostSlug)
            ->firstOrFail();

        if ($captcha = $this->getCaptchaObject()) {
            $captcha->runCaptchaBeforeShowingPosts($request, $forum_post);
        }

        return view("forum::single_post", [
            'post' => $forum_post,
            // the default scope only selects approved comments, ordered by id
            'comments' => $forum_post->comments()
                ->with("user")
                ->get(),
            'captcha' => $captcha,
        ]);
    }






}
