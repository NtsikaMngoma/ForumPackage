<?php

namespace MergeAfrica\Forum\Controllers;

use App\Http\Controllers\Controller;
use MergeAfrica\Forum\Events\CategoryAdded;
use MergeAfrica\Forum\Events\CategoryEdited;
use MergeAfrica\Forum\Events\CategoryWillBeDeleted;
use MergeAfrica\Forum\Helpers;
use MergeAfrica\Forum\Middleware\UserCanManageForumPosts;
use MergeAfrica\Forum\Models\ForumCategory;
use MergeAfrica\Forum\Requests\DeleteForumCategoryRequest;
use MergeAfrica\Forum\Requests\StoreForumCategoryRequest;
use MergeAfrica\Forum\Requests\UpdateForumCategoryRequest;

/**
 * Class ForumCategoryAdminController
 * @package MergeAfrica\Forum\Controllers
 */
class ForumCategoryAdminController extends Controller
{
    /**
     * ForumCategoryAdminController constructor.
     */
    public function __construct()
    {
        $this->middleware(UserCanManageForumPosts::class);
    }

    /**
     * Show list of categories
     *
     * @return mixed
     */
    public function index(){

        $categories = ForumCategory::orderBy("category_name")->paginate(25);
        return view("forum_admin::categories.index")->withCategories($categories);
    }

    /**
     * Show the form for creating new category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create_category(){

        return view("forum_admin::categories.add_category");

    }

    /**
     * Store a new category
     *
     * @param StoreForumCategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store_category(StoreForumCategoryRequest $request){
        $new_category = new ForumCategory($request->all());
        $new_category->save();

        Helpers::flash_message("Saved new category");

        event(new CategoryAdded($new_category));
        return redirect( route('forum.admin.categories.index') );
    }

    /**
     * Show the edit form for category
     * @param $categoryId
     * @return mixed
     */
    public function edit_category($categoryId){
        $category = ForumCategory::findOrFail($categoryId);
        return view("forum_admin::categories.edit_category")->withCategory($category);
    }

    /**
     * Save submitted changes
     *
     * @param UpdateForumCategoryRequest $request
     * @param $categoryId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update_category(UpdateForumCategoryRequest $request, $categoryId){
        /** @var ForumCategory $category */
        $category = ForumCategory::findOrFail($categoryId);
        $category->fill($request->all());
        $category->save();

        Helpers::flash_message("Saved category changes");
        event(new CategoryEdited($category));
        return redirect($category->edit_url());
    }

    /**
     * Delete the category
     *
     * @param DeleteForumCategoryRequest $request
     * @param $categoryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy_category(DeleteForumCategoryRequest $request, $categoryId){

        /* Please keep this in, so code inspections don't say $request was unused. Of course it might now get marked as left/right parts are equal */
        $request=$request;

        $category = ForumCategory::findOrFail($categoryId);
        event(new CategoryWillBeDeleted($category));
        $category->delete();

        return view ("forum_admin::categories.deleted_category");

    }

}
